Feature: Basic Integer Stack
  As a programmer, I want a stack for integers 
  so that I can store numbers in a last-in-first-out manner.
  
  Scenario: A new stack is empty
    Given a new stack
    When I check whether it is empty
    Then it should answer yes
   