package intStack;

import io.cucumber.java.en.*;

import static org.junit.Assert.*;

public class IntStackStepdefs {
	private IntStack theStack;
	private Boolean actualBooleanAnswer;

	@Given("^a new stack$")
	public void newStack() {
		theStack = new IntStack();
	}

	@When("^I check whether it is empty$")
	public void checkWhetherItIsEmpty() {
		actualBooleanAnswer = theStack.isEmpty();
	}
	@Then("^it should answer yes$")
	public void itShouldAnswerYes() {
		assertTrue(actualBooleanAnswer);
	}
}